import java.util.Arrays;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
//      Lfraction first = new Lfraction(0, 3);
//      first.inverse();
//      System.out.println(first);
      Lfraction second = new Lfraction(4, -8);
      Lfraction same = new Lfraction(-1, 2);
      System.out.println(second.hashCode() == same.hashCode());
//      System.out.println(second);
//      second.divideBy(first);
//      Lfraction third = new Lfraction(-6, 27);
//      Lfraction fourth = new Lfraction(-1, 0);
//      Lfraction fifth = new Lfraction(-17, 100);
//      Lfraction sixth = new Lfraction(85158400,-360633600);
//      System.out.println(sixth);
//      Lfraction.valueOf("5 / 0");


   }

   private final Long numerator;
   private final Long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0) {
         throw new RuntimeException("Denominator can't be zero");
      } else {
         Long[] nums = normalizeFraction(a, b);
         this.numerator = nums[0];
         this.denominator = nums[1];
      }
   }

   private static Long[] normalizeFraction(Long numerator, Long denominator) {
      Long[] reduced = new Long[] {numerator, denominator};

      if (denominator == 0) {
         throw new RuntimeException("Denominator can't be zero!");
      } else if (numerator == 0) {
         reduced[1] = 1L;
         return reduced;
      }

      long smallerAbsNumber = Math.min(Math.abs(numerator), Math.abs(denominator));
      Long biggerAbsNumber = Math.max(Math.abs(numerator), Math.abs(denominator));
      long divider = 2;

      while (Math.min(Math.abs(numerator), Math.abs(denominator)) > 1
              && divider <= smallerAbsNumber) {

         if (numerator.doubleValue() % divider == 0 && denominator.doubleValue() % divider == 0) {
            numerator /= divider;
            denominator /= divider;
         } else {
            divider++;
         }
      }

      if (denominator < 0) {
         reduced[0] = -numerator;
         reduced[1] = -denominator;
      } else {
         reduced[0] = numerator;
         reduced[1] = denominator;
      }

      return reduced;
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return this.numerator + " / " + this.denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m.getClass().equals(this.getClass())) {
         Lfraction other = (Lfraction) m;
         return this.compareTo(other) == 0;
      }
      return false;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      Long[] reduced = normalizeFraction(this.numerator, this.denominator);
      return Arrays.hashCode(reduced);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long newDenominator = this.denominator * m.getDenominator();
      long a = newDenominator / this.denominator * this.numerator;
      long b = newDenominator / m.getDenominator() * m.getNumerator();
      Long[] result = normalizeFraction(a + b, newDenominator);
      return new Lfraction(result[0], result[1]);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      Long a = this.numerator * m.getNumerator();
      Long b = this.denominator * m.getDenominator();
      Long[] result = normalizeFraction(a, b);

      return new Lfraction(result[0], result[1]);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.numerator == 0) {
         throw new RuntimeException("Can't inverse! New denominator would be zero!");
      }
      return new Lfraction(this.denominator, this.numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.numerator, this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.getNumerator() == 0) {
         throw new RuntimeException("Can't divide by zero!");
      }
      return times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (this.numerator.equals(m.numerator) && this.denominator.equals(m.denominator)) {
         return 0;
      } else if (this.numerator * m.getDenominator() - this.denominator * m.getNumerator() < 0) {
         return -1;
      }
      return 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerator / this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      long integerPart = integerPart();
      return new Lfraction(this.numerator - integerPart * this.denominator, this.denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return this.numerator.doubleValue() / this.denominator.doubleValue();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f * d), d);
   }

   public Lfraction pow(long n) {
      if (n == 0) {
         return new Lfraction(1,1);
      } else if (n == 1) {
         return new Lfraction(this.numerator, this.denominator);
      } else if (n == -1) {
         return this.inverse();
      } else if (n > 1) {
         return this.times(this.pow(n-1));
      }

      return this.pow(Math.negateExact(n)).inverse();
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      Long a = null;
      Long b = null;
      String[] str = s.split(" / ");
      try {
         a = Long.parseLong(str[0]);
         b = Long.parseLong(str[1]);
      } catch (NumberFormatException e) {
         throw new RuntimeException("'" + s + "' is invalid argument, must be in form 'number/number'");
      }
      if (b == 0) {
         throw new RuntimeException("'" + s + "' is invalid argument, denominator can't be zero");
      }
      return new Lfraction(a, b);
   }
}

