//import java.util.ArrayList;
//import java.util.Arrays;
//
///** This class represents fractions of form n/d where n and d are long integer
// * numbers. Basic operations and arithmetics for fractions are provided.
// */
//public class LfractionOther implements Comparable<Lfraction> {
//
//    /** Main method. Different tests. */
//    public static void main (String[] param) {
////      Lfraction first = new Lfraction(0, 3);
//        Lfraction second = new Lfraction(250, 1200);
////      Lfraction third = new Lfraction(-6, 27);
////      Lfraction fourth = new Lfraction(-1, 0);
////      Lfraction fifth = new Lfraction(-17, 100);
////      Lfraction sixth = new Lfraction(84, -840);
////      Lfraction seventh = new Lfraction(264, 16);
////      Lfraction eighth = new Lfraction(0, -1);
//    }
//
//    private final Long numerator;
//    private final Long denominator;
//
//    /** Constructor.
//     * @param a numerator
//     * @param b denominator > 0
//     */
//    public LfractionOther (long a, long b) {
//        if (b == 0) {
//            throw new RuntimeException("Denominator can't be zero");
//        }
//        Long[] nums = normalizeFraction(a, b);
//
//        if (a == 0) {
//            this.numerator = 0L;
//            this.denominator = 1L;
//        } else if (b < 0){
//            this.numerator = -nums[0];
//            this.denominator = -nums[1];
//        } else {
//            this.numerator = nums[0];
//            this.denominator = nums[1];
//        }
////      System.out.println(this.numerator);
////      System.out.println(this.denominator);
//    }
//
//    public static Long[] normalizeFraction(Long numerator, Long denominator) {
//        Long[] reduced = new Long[] {numerator, denominator};
//
//        if (denominator == 0) {
//            throw new RuntimeException("Denominator can't be zero!");
//        } else if (numerator == 0) {
//            reduced[1] = 1L;
//            return reduced;
//        }
//
//        long commonMultiplier = getCommonMultiplier(numerator, denominator);
//        reduced[0] /= commonMultiplier;
//        reduced[1] /= commonMultiplier;
//
////      long minimumElement = Math.min(Math.abs(reduced[0]), Math.abs(reduced[1]));
////      for (long i = minimumElement; i > 1; i--) {
////         if (denominator.doubleValue() % i == 0 && numerator.doubleValue() % i == 0) {
////            reduced[0] = numerator / i;
////            reduced[1] = denominator / i;
////            return reduced;
////         }
////      }
//        return reduced;
//    }
//
//    public static long getCommonMultiplier(Long numerator, Long denominator) {
//        ArrayList<Integer> first = getPrimes(Math.abs(numerator));
//        ArrayList<Integer> second = getPrimes(Math.abs(denominator));
//        long commonMultiplier = 1L;
//
//        for (Integer num : first) {
//            if (second.contains(num)) {
//                second.remove(num);
//                commonMultiplier *= num;
//            }
//        }
//
//        return commonMultiplier;
//    }
//
//    public static ArrayList<Integer> getPrimes(Long originalNumber) {
//        ArrayList<Integer> primes = new ArrayList<>();
//        long number = originalNumber;
//        int divider = 2;
//
//        while (number > 1) {
//            if (divider > Math.sqrt(originalNumber) && primes.isEmpty()){
//                return primes;
//            } else if (number % divider == 0) {
//                number /= divider;
//                primes.add(divider);
//            } else {
//                divider++;
//            }
//        }
////      for (long prime : primes) {
////         System.out.println(prime);
////      }
////      System.out.println("----------------");
//        return primes;
//    }
//
//    /** Public method to access the numerator field.
//     * @return numerator
//     */
//    public long getNumerator() {
//        return this.numerator;
//    }
//
//    /** Public method to access the denominator field.
//     * @return denominator
//     */
//    public long getDenominator() {
//        return this.denominator;
//    }
//
//    /** Conversion to string.
//     * @return string representation of the fraction
//     */
//    @Override
//    public String toString() {
//        return this.numerator + " / " + this.denominator;
//    }
//
//    /** Equality test.
//     * @param m second fraction
//     * @return true if fractions this and m are equal
//     */
//    @Override
//    public boolean equals (Object m) {
//        if (m.getClass().equals(this.getClass())) {
//            Lfraction other = (Lfraction) m;
//            return this.numerator * other.getDenominator() == this.denominator * other.getNumerator();
//        }
//        return false;
//    }
//
//    /** Hashcode has to be equal for equal fractions.
//     * @return hashcode
//     */
//    @Override
//    public int hashCode() {
//        Long[] reduced = normalizeFraction(this.numerator, this.denominator);
//        return Arrays.hashCode(reduced);
//    }
//
//    /** Sum of fractions.
//     * @param m second addend
//     * @return this+m
//     */
//    public Lfraction plus (Lfraction m) {
//        long newDenominator = this.denominator * m.getDenominator();
//        long a = newDenominator / this.denominator * this.numerator;
//        long b = newDenominator / m.getDenominator() * m.getNumerator();
//        Long[] result = normalizeFraction(a + b, newDenominator);
//        return new Lfraction(result[0], result[1]);
//    }
//
//    /** Multiplication of fractions.
//     * @param m second factor
//     * @return this*m
//     */
//    public Lfraction times (Lfraction m) {
//        Long a = this.numerator * m.getNumerator();
//        Long b = this.denominator * m.getDenominator();
//        Long[] result = normalizeFraction(a, b);
//
//        return new Lfraction(result[0], result[1]);
//    }
//
//    /** Inverse of the fraction. n/d becomes d/n.
//     * @return inverse of this fraction: 1/this
//     */
//    public Lfraction inverse() {
//        return new Lfraction(this.denominator, this.numerator);
//    }
//
//    /** Opposite of the fraction. n/d becomes -n/d.
//     * @return opposite of this fraction: -this
//     */
//    public Lfraction opposite() {
//        return new Lfraction(-this.numerator, this.denominator);
//    }
//
//    /** Difference of fractions.
//     * @param m subtrahend
//     * @return this-m
//     */
//    public Lfraction minus (Lfraction m) {
//        long newDenominator = this.denominator * m.getDenominator();
//        long a = newDenominator / this.denominator * this.numerator;
//        long b = newDenominator / m.getDenominator() * m.getNumerator();
//        Long[] result = normalizeFraction(a - b, newDenominator);
//        return new Lfraction(result[0], result[1]);
//    }
//
//    /** Quotient of fractions.
//     * @param m divisor
//     * @return this/m
//     */
//    public Lfraction divideBy (Lfraction m) {
//        if (m.getNumerator() == 0) {
//            throw new RuntimeException("Can't divide by zero!");
//        }
//        long a = this.numerator * m.getDenominator();
//        long b = this.denominator * m.getNumerator();
//        Long[] reduced = normalizeFraction(a, b);
//        return new Lfraction(a, b);
//    }
//
//    /** Comparision of fractions.
//     * @param m second fraction
//     * @return -1 if this < m; 0 if this==m; 1 if this > m
//     */
//    @Override
//    public int compareTo (Lfraction m) {
//        if (equals(m)) {
//            return 0;
//        } else if (this.numerator * m.getDenominator() - this.denominator * m.getNumerator() < 0) {
//            return -1;
//        }
//        return 1;
//    }
//
//    /** Clone of the fraction.
//     * @return new fraction equal to this
//     */
//    @Override
//    public Object clone() throws CloneNotSupportedException {
//        return new Lfraction(this.numerator, this.denominator);
//    }
//
//    /** Integer part of the (improper) fraction.
//     * @return integer part of this fraction
//     */
//    public long integerPart() {
//        return this.numerator / this.denominator;
//    }
//
//    /** Extract fraction part of the (improper) fraction
//     * (a proper fraction without the integer part).
//     * @return fraction part of this fraction
//     */
//    public Lfraction fractionPart() {
//        long integerPart = integerPart();
//        return new Lfraction(this.numerator - integerPart * this.denominator, this.denominator);
//    }
//
//    /** Approximate value of the fraction.
//     * @return numeric value of this fraction
//     */
//    public double toDouble() {
//        return this.numerator.doubleValue() / this.denominator.doubleValue();
//    }
//
//    /** Double value f presented as a fraction with denominator d > 0.
//     * @param f real number
//     * @param d positive denominator for the result
//     * @return f as an approximate fraction of form n/d
//     */
//    public static Lfraction toLfraction (double f, long d) {
//        return new Lfraction(Math.round(f * d), d);
//    }
//
//    /** Conversion from string to the fraction. Accepts strings of form
//     * that is defined by the toString method.
//     * @param s string form (as produced by toString) of the fraction
//     * @return fraction represented by s
//     */
//    public static Lfraction valueOf (String s) {
//        String[] str = s.split(" / ");
//        return new Lfraction(Long.parseLong(str[0]), Long.parseLong(str[1]));
//    }
//}
